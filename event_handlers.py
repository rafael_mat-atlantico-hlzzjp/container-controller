import tkinter as tk
from commands import nerdctl_ps, nerdctl_compose_down, nerdctl_compose_up, nerdctl_start, nerdctl_stop

def refresh_containers(listbox, env_path:str):
    listbox.delete(0, tk.END)
    for container in nerdctl_ps(env_path):
        listbox.insert(tk.END, container["name"])
    return listbox

def compose_up(listbox, env_path:str):
    output = nerdctl_compose_up(env_path)
    listbox_updated = refresh_containers(listbox, env_path)
    return output

def compose_down(listbox, env_path:str):
    output = nerdctl_compose_down(env_path)
    listbox_updated = refresh_containers(listbox, env_path)
    return output

def restart_container(listbox, container_index, env_path:str):
    container_name = listbox.get(container_index)
    output = nerdctl_stop(container_name, env_path)
    output = nerdctl_start(container_name, env_path)
    listbox_updated = refresh_containers(listbox, env_path)
    return True