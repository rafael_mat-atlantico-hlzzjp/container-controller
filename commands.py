import json
import subprocess

ps_format = "'{\"id\":\"{{ .ID }}\",\"name\":\"{{ .Names }}\"}'"

commands = {
    "ps": ["nerdctl","ps","--no-trunc","--format",ps_format],
    "compose_up": ["nerdctl","compose","up","-d"],
    "compose_down": ["nerdctl","compose","down"],
    "start": ["nerdctl","start","<container_name>"],
    "stop": ["nerdctl","stop","<container_name>"]
}

def nerdctl_ps(env_path):
    result = subprocess.run(commands["ps"], cwd=env_path, stdout=subprocess.PIPE)
    output = result.stdout.decode("utf-8").replace("\'","").split("\n")
    output.pop(-1)
    return [json.loads(x) for x in output]

def nerdctl_compose_up(env_path):
    output = subprocess.run(commands["compose_up"], cwd=env_path)
    return output

def nerdctl_compose_down(env_path):
    output = subprocess.run(commands["compose_down"], cwd=env_path)
    return output

def nerdctl_start(container_name, env_path):
    commands["start"][2] = container_name
    print(commands["start"])
    output = subprocess.run(commands["start"], cwd=env_path)
    return output

def nerdctl_stop(container_name, env_path):
    commands["stop"][2] = container_name
    print(commands["stop"])
    output = subprocess.run(commands["stop"], cwd=env_path)
    return output