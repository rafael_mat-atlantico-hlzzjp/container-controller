import sys
import tkinter as tk
from settings import ENV_PATH
from event_handlers import refresh_containers, compose_up, compose_down, restart_container

# default values
font_default = ("Arial", "10")
font_default_title = ("Arial", "15")
font_default_btn = ("Arial", "10")
btn_default_width = 15

def main(args):

    # Frames config - Application
    root = tk.Tk()
    root.title("Container Controller")

    # Frames config - frame_root
    frame_root = tk.Frame(root, relief=tk.SUNKEN, borderwidth=1)
    frame_root.pack()

    # Frames config - frame_env_path
    frame_env_path = tk.Frame(frame_root, pady= 20)
    frame_env_path.pack()

    # Frames config - frame_containers
    frame_containers = tk.Frame(frame_root)
    frame_containers.pack()

    # Frames config - frame_output
    frame_output = tk.Frame(frame_root)
    frame_output.pack()

    # First Section: Env Path
    label_env_path = tk.Label(frame_env_path, text="Env Path", font=font_default)
    label_env_path.grid(row=0,column=0, padx= 10, pady=10)

    entry_env_path = tk.Entry(frame_env_path)
    entry_env_path.insert(tk.END, ENV_PATH)
    entry_env_path["width"] = 30
    entry_env_path["font"] = font_default
    entry_env_path.grid(row=0, column=1)

    # Config scrollbar and Listbox
    scrollbar = tk.Scrollbar(frame_containers)
    list_containers = tk.Listbox(frame_containers, yscrollcommand = scrollbar.set)
    scrollbar.config(command=list_containers.yview)
    list_containers = refresh_containers(list_containers, entry_env_path.get())
    list_containers.pack(side=tk.LEFT, fill=tk.BOTH, padx=5)
    scrollbar.pack(side = tk.RIGHT, fill = tk.Y)

    btn_env_up = tk.Button(frame_env_path, text="Compose Up", font=font_default_btn, width=btn_default_width)
    btn_env_up["command"] = lambda : compose_up(list_containers, entry_env_path.get())
    btn_env_up.grid(row=0,column=2, padx= 10, pady=10)

    btn_env_down = tk.Button(frame_env_path, text="Compose Down", font=font_default_btn, width=btn_default_width)
    btn_env_down["command"] = lambda : compose_down(list_containers, entry_env_path.get())
    btn_env_down.grid(row=0,column=3, padx= 10, pady=10)

    # Second Section: Containers List
    label_container_list = tk.Label(frame_containers, text="Container List", font=font_default_title)
    label_container_list.pack(fill=tk.BOTH, padx=5, pady=10)

    btn_restart = tk.Button(frame_containers, text="Restart", font=font_default_btn, width=btn_default_width)
    btn_restart["command"] = lambda : restart_container(list_containers, list_containers.curselection(), entry_env_path.get())
    btn_restart.pack(side=tk.RIGHT, fill=tk.BOTH, padx=5)

    btn_refresh = tk.Button(frame_containers, text="Refresh", font=font_default_btn, width=btn_default_width)
    btn_refresh["command"] = lambda : refresh_containers(list_containers, entry_env_path.get())
    btn_refresh.pack(side=tk.RIGHT, fill=tk.BOTH, padx=5)

    label_container_output = tk.Label(frame_output, text="Container Controller", font=font_default)
    label_container_output.pack(fill=tk.BOTH, padx=20, pady=20)

    root.mainloop()
    return 0

if __name__ == '__main__':
    print("Starting Container Controller...")
    sys.exit(main(sys.argv))